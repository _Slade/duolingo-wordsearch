import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';

import { PuzzleApiService } from '../services/puzzle-api.service';
import { PuzzleSpec } from '../shared/types';
import { WordsearchGridComponent as GridComponent } from '../wordsearch-grid/wordsearch-grid.component';
import { unabbreviateLanguage } from '../shared/util';

@Component({
  selector: 'app-wordsearch',
  templateUrl: './wordsearch.component.html',
  styleUrls: ['./wordsearch.component.scss']
})
export class WordsearchComponent implements OnInit {

  puzzle$: Observable<PuzzleSpec>;
  @ViewChild('grid') grid: GridComponent;
  solved: boolean = false;

  constructor(private puzzleApi: PuzzleApiService) { }

  unabbreviateLanguage = unabbreviateLanguage;

  ngOnInit(): void {
    this.getNewPuzzle();
  }

  getNewPuzzle(): void {
    this.solved = false;
    this.puzzle$ = this.puzzleApi.getRandomPuzzle();
  }

  revealSolution(): void {
    if (this.grid != null) {
      this.grid.revealSolution();
    }
  }

}
