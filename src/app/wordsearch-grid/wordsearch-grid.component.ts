import { Component, ElementRef, EventEmitter, Input, Output, NgZone, ViewChild } from '@angular/core';
import { Group, Item, Layer, PaperScope, Path, Point, PointText, Project, Rectangle, Shape, Size } from 'paper';
import { MouseEvent as PaperMouseEvent } from 'paper';

import { Coord, PuzzleSpec, Selection } from '../shared/types';
import { angleBetween, midpoint, parseWordLocations, selectionHasWord, wordToString } from '../shared/util';

enum Layers {
  Background = 0,
  Cell,
  Answer,
  Selection,
}

const SELECTION_COLOR = '#007BFF';

@Component({
  selector: 'app-wordsearch-grid',
  templateUrl: './wordsearch-grid.component.html',
  styleUrls: ['./wordsearch-grid.component.scss']
})
export class WordsearchGridComponent {

  private _puzzle: PuzzleSpec;

  // Dimensions for graphics
  // Base font size. Determines the size of the grid, because cells are sized
  // to fit the letters.
  @Input() fontSize: number;
  readonly cellPaddingFactor = 0.25;
  private N?: number; // Number of cells
  private size = 0;
  private selection: Selection = { active: false };
  private answers?: Set<string>;
  private foundAnswers = 0;

  // Elements for rendering
  @ViewChild('container')
  private container: ElementRef;
  private canvas: HTMLCanvasElement;
  private project: Project;
  private paper: PaperScope;

  private _solved: boolean;
  @Output() solvedChange = new EventEmitter<boolean>();
  @Input() set solved(value: boolean) {
    this._solved = value;
    this.solvedChange.emit(value);
  }
  get solved(): boolean { return this._solved; }

  constructor(private ngZone: NgZone) {
    this.paper = new PaperScope();
    this.initialize();
  }

  private initialize(): void {
    this.N = undefined;
    this.answers = undefined;
    this.foundAnswers = 0;
    this.selection = { active: false };
    this.size = 0;
  }

  @Input()
  set puzzle(puzzle: PuzzleSpec) {
    this.initialize();
    this._puzzle = puzzle;
    this.answers = new Set<string>(
      Object.keys(puzzle.word_locations).map(
        wordLoc => wordToString(parseWordLocations(wordLoc))
      )
    );
    this.renderPuzzle(this._puzzle);
  }
  get puzzle(): PuzzleSpec { return this._puzzle; }

  get cellSize(): number {
    return this.fontSize + this.fontSize * this.cellPaddingFactor;
  }

  private getLayer(layer: Layers): Layer {
    return this.project.layers[layer];
  }

  /**
   * Clean up resources associated with the currently rendered puzzle
   */
  private clearPuzzle(): void {
    const c = this.container.nativeElement;
    while (c != null && c.firstChild != null) {
      c.removeChild(c.firstChild);
    }
    if (this.project != null) {
      const idx = this.paper.projects.indexOf(this.project);
      if (idx !== -1) {
        this.paper.projects.splice(idx, 1);
      }
    }
  }

  private renderPuzzle(puzzle: PuzzleSpec): void {
    this.clearPuzzle();
    this.canvas = document.createElement('canvas');
    this.N = puzzle.character_grid.length;
    this.size = this.N * this.cellSize;
    this.canvas.width = this.size;
    this.canvas.height = this.size;
    this.container.nativeElement.appendChild(this.canvas);
    this.project = new Project(this.canvas);
    try {
      this.render();
    }
    catch (e) {
      console.log('Error rendering puzzle:', e);
    }
  }

  public revealSolution(): void {
    this.drawAnswerBubbles();
    this.solved = true;
  }

  private render(): void {
    this.createLayers();
    this.drawBackground();
    this.drawGrid();
    this.setUpMouseHandlers();
  }

  private getCellAt(x: number, y: number): Item {
    const hit = this.getLayer(Layers.Cell)
      .hitTest(new Point(x * this.cellSize, y * this.cellSize));
    if (hit != null) {
      return hit.item;
    }
    else {
      throw new Error(`Failed to find cell at (${x}, ${y})`);
    }
  }

  private drawAnswerBubbles(): void {
    this.getLayer(Layers.Answer).activate();
    for (const locString of Object.keys(this.puzzle.word_locations)) {
      const coords = parseWordLocations(locString);
      this.drawAnswerBubble(coords.start, coords.end);
    }
  }

  /**
   * Draw an answer bubble enclosing the line of cells between (ax, ay) and
   * (bx, by).
   */
  private drawAnswerBubble(start: Coord, end: Coord): void {
    const startCell = this.getCellAt(...start);
    const endCell   = this.getCellAt(...end);
    const center    = midpoint(startCell.position, endCell.position);
    const width     = startCell.position.getDistance(endCell.position)
      + this.cellSize - 6;
    const height    = this.cellSize - 6;
    const rectangle = new Rectangle(
      center.subtract(new Point(width / 2, height / 2)),
      new Size(width, height),
    );
    const path = new Path.Rectangle(rectangle, this.fontSize / 2);
    path.rotate(angleBetween(startCell.position, endCell.position));
    path.strokeColor = SELECTION_COLOR;
    path.strokeWidth = 2;
  }

  private createLayers(): void {
    const newLayer = (index: number, name: string) =>
      this.project.insertLayer(index, new Layer({ name }));
    newLayer(Layers.Background, Layers[Layers.Background]);
    newLayer(Layers.Selection, Layers[Layers.Selection]);
    newLayer(Layers.Cell, Layers[Layers.Cell]);
  }

  private drawBackground(): void {
    this.getLayer(Layers.Background).activate();
    const background = Shape.Rectangle(
      new Point(0, 0),
      new Size(this.size, this.size)
    );
    background.fillColor = 'white';
  }

  private drawGrid(): void {
    if (this.N == null) {
      throw new Error('Illegal state: drawGrid() called before N is set');
    }
    for (let x = 0; x < this.N; x++) {
      for (let y = 0; y < this.N; y++) {
        const point = new Point(x * this.cellSize, y * this.cellSize);
        this.drawCell(
          point,
          this.cellSize,
          this.puzzle.character_grid[y][x].toUpperCase(),
          { x, y },
        );
      }
    }
  }

  private drawCell(point: Point, size: number, letter: string, data?: Object): void {
    const borderSize = new Size(size, size);
    const cellSize   = borderSize.subtract(2);
    const cellOffset = new Point(1, 1);
    const textOffset = new Point(
      this.cellSize / 2,
      this.cellSize - (this.fontSize * this.cellPaddingFactor)
    );
    this.getLayer(Layers.Cell).activate();
    const cell = new Group([
      this.makeCellBorder(point, borderSize),
      this.makeCellBody(point.add(cellOffset), cellSize),
      this.makeLetter(point.add(textOffset), letter),
    ]);
    if (data != null) {
      cell.data = data;
    }
  }

  private makeCellBody = (point: Point, size: Size): Item => Shape.Rectangle({
    point:     point,
    size:      size,
    fillColor: 'white',
    opacity:   0,
  });

  private makeCellBorder = (point: Point, borderSize: Size): Item => Shape.Rectangle({
    point: point,
    size: borderSize,
    strokeColor: 'black',
  });

  private makeLetter = (point: Point, letter: string): Item => new PointText({
    point:         point,
    content:       letter,
    fillColor:     'black',
    justification: 'center',
    fontFamily:    'mono',
    fontSize:      this.fontSize,
  });

  private setUpMouseHandlers(): void {
    this.getLayer(Layers.Cell).children.forEach(cell => {
      cell.onMouseDown = (event) => {
        this.ngZone.run(this.beginSelection.bind(this, event))
      };
      cell.onMouseUp = (event) => {
        this.ngZone.run(this.endSelection.bind(this, event))
      };
    });
  }

  private beginSelection(event: PaperMouseEvent): void {
    if (this.solved) {
      return;
    }
    const coords = WordsearchGridComponent.getTargetCoords(event);
    if (coords != null) {
      this.selection.active = true;
      this.selection.start = coords;
      event.stopPropagation();
    }
  }

  private endSelection(event: PaperMouseEvent): void {
    if (this.solved) {
      return;
    }
    const coords = WordsearchGridComponent.getTargetCoords(event);
    if (this.selection.active && coords != null) {
      this.selection.end = coords;
      event.stopPropagation();
    }
    this.validateSelection(this.selection);
  }

  private static getTargetCoords(event: PaperMouseEvent): Coord | undefined {
    if (event.target != null) {
      const { target } = event;
      if ('data' in target && 'x' in target.data && 'y' in target.data) {
        return [ target.data.x, target.data.y ];
      }
    }
    return undefined;
  }

  private validateSelection(selection: Selection): void {
    if (this.answers == null) {
      throw new Error(
        'Bad state: answers is undefined but validateSelection() was called'
      );
    }
    if (selection.active && selectionHasWord(selection)
      && this.answers.has(wordToString(selection))) {
      this.drawAnswerBubble(selection.start, selection.end);
      this.foundAnswers++;
    }
    selection.start  = undefined;
    selection.end    = undefined;
    selection.active = false;

    if (this.foundAnswers === this.answers.size) {
      this.solved = true;
    }
  }

}
