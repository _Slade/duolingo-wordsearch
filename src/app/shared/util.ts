import { Point } from 'paper';

import { Selection, Word } from './types';

export function randomElement<T = any>(array: T[]): T | undefined {
  if (array.length === 0) {
    return;
  }
  return array[Math.floor(Math.random() * array.length)];
}

const languageMappings: { [lang: string]: string } = {
  es: 'Spanish',
  en: 'English',
};
export function unabbreviateLanguage(lang: string): string | undefined {
  return languageMappings[lang];
}

export const midpoint = (a: Point, b: Point): Point =>
  new Point((a.x + b.x) / 2, (a.y + b.y) / 2);

export const angleBetween = (a: Point, b: Point): number =>
  (Math.atan2(b.y - a.y, b.x - a.x)) * 180 / Math.PI;

export function parseWordLocations(locString: string): Word {
  const coords = locString.split(',').map(num => parseInt(num, 10));
  if (coords.length % 2 !== 0 || coords.length < 4) {
    throw new Error('Illegal format for word_locations');
  }
  return {
    start: [coords[0], coords[1]],
    end: [coords[coords.length - 2], coords[coords.length - 1]],
  };
}

export function wordToString(word: Word): string {
  return [ ...word.start, ...word.end ].join(',');
}

export function selectionHasWord(selection: Word | Selection): selection is Word {
  return (selection as Word).start != null && (selection as Word).end != null;
}
