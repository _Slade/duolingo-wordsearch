import { Point } from 'paper';

export type Coord = [number, number];

export interface PuzzleSpec {
  source_language: string;
  target_language: string;
  word: string;
  character_grid: string[][];
  word_locations: { [locations: string]: string };
}

export interface PaperMouseEvent extends MouseEvent {
  point: Point;
  target: any;
}

export interface Word {
  start: Coord;
  end: Coord;
}

export interface Selection extends Partial<Word> {
  active: boolean;
}
